@extends('layouts.app')

@section('section-header', 'Shops')

@section('section-body')
    <div class="card">
        <div class="card-header">
            <h4></h4>
            <div class="card-header-form">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <a href="/shops/create" class="btn btn-primary ml-2">Create shop</a>
        </div>
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tr>
                        <th>#</th>
                        <th>Logo</th>
                        <th>Name</th>
                        <th>City</th>
                        <th>Owner</th>
                        <th>Link</th>
                        <th>Action</th>
                    </tr>
                    @foreach($shops as $shop)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>
                                @if($shop->logo)
                                    <img src="/storage/{{ $shop->logo }}" width="70px"/>
                                @else
                                    N/A
                                @endif
                            </td>
                            <td>{{ $shop->name }}</td>
                            <td>{{ $shop->city }}</td>
                            <td>{{ $shop->user->name }}</td>
                            <td>https://online.shop/{{ $shop->link }}</td>
                            <td><a href="/shops/{{ $shop->id }}/edit" class="btn btn-icon icon-left btn-info"><i class="fas fa-pencil-alt"></i> Edit</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection