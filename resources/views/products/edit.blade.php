@extends('layouts.app')

@section('section-header', 'Shops')

@section('section-body')
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Edit shop</h4>
            </div>
            <div class="card-body">
                <form action="/shops/{{ $shop->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control {{ $errors->first('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name') ? old('name') : $shop->name }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('name')}}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control {{ $errors->first('city') ? 'is-invalid' : '' }}" name="city" value="{{ old('city') ? old('city') : $shop->city }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('city')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Link</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">https://online.shop/</div>
                            </div>
                            <input type="text" class="form-control {{ $errors->first('link') ? 'is-invalid' : '' }}" name="link" value="{{ old('link') ? old('link') : $shop->link }}" required>

                            <div class="invalid-feedback">
                                {{$errors->first('link')}}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="avatar">Logo</label><br>

                        Current logo: <br>
                        @if($shop->logo)
                            <img
                            src="{{ asset('storage/' . $shop->logo) }}"
                            width="120px" />
                            <br>
                            @else
                            <br>No logo<br>
                        @endif

                        <br>
                        <div class="custom-file">
                            <input id="logo" name="logo" type="file" class="custom-file-input">
                            <label class="custom-file-label" for="logo">Choose file</label>
                        </div>

                        <small class="text-muted">Kosongkan jika tidak ingin mengubah logo</small>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
