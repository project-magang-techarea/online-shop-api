@extends('layouts.app')

@section('section-header', 'products')

@section('section-body')
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Create product</h4>
            </div>
            <div class="card-body">
                <form action="/products" method="POST">
                    @csrf

                    <div class="form-group">
                        <label>User</label>
                        <select class="form-control" name="user">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control {{ $errors->first('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('name')}}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control {{ $errors->first('city') ? 'is-invalid' : '' }}" name="city" value="{{ old('city') }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('city')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">https://online.product/</div>
                            </div>
                            <input type="text" class="form-control {{ $errors->first('link') ? 'is-invalid' : '' }}" name="link" value="{{ old('link') }}" required>

                            <div class="invalid-feedback">
                                {{$errors->first('link')}}
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
