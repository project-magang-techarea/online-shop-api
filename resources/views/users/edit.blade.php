@extends('layouts.app')

@section('section-header', 'Users')

@section('section-body')
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Create user</h4>
            </div>
            <div class="card-body">
                <form action="/users/{{ $user->id }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control {{ $errors->first('name') ? 'is-invalid' : '' }}" name="name" value="{{ $user->name }}">

                        <div class="invalid-feedback">
                            {{$errors->first('name')}}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control {{ $errors->first('email') ? 'is-invalid' : '' }}" name="email" value="{{ $user->email }}" required disabled> 

                        <div class="invalid-feedback">
                            {{$errors->first('email')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control {{ $errors->first('phone_number') ? 'is-invalid' : '' }}" name="phone_number" value="{{ $user->phone_number }}" required disabled> 

                        <div class="invalid-feedback">
                            {{$errors->first('phone_number')}}
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection