@extends('layouts.app')

@section('section-header', 'Users')

@section('section-body')
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Create user</h4>
            </div>
            <div class="card-body">
                <form action="/users" method="POST">
                    @csrf

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control {{ $errors->first('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('name')}}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control {{ $errors->first('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('email')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control {{ $errors->first('phone_number') ? 'is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" required>

                        <div class="invalid-feedback">
                            {{$errors->first('phone_number')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control {{ $errors->first('password') ? 'is-invalid' : '' }}" name="password" required>

                        <div class="invalid-feedback">
                            {{$errors->first('password')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Confirmation Password</label>
                        <input type="password" class="form-control {{ $errors->first('password_confirmation') ? 'is-invalid' : '' }}" name="password_confirmation" required>

                        <div class="invalid-feedback">
                            {{$errors->first('password_confirmation')}}
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
