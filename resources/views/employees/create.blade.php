@extends('layouts.app')

@section('section-header', 'Employees')

@section('section-body')
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Create employee</h4>
            </div>
            <div class="card-body">
                <form action="/employees" method="POST">
                    @csrf

                    <div class="form-group">
                        <label>User</label>
                        <select class="form-control {{ $errors->first('user') ? 'is-invalid' : '' }}" name="user">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>

                        <div class="invalid-feedback">
                            {{$errors->first('user')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Shop</label>
                        <select class="form-control {{ $errors->first('shop') ? 'is-invalid' : '' }}" name="shop">
                            @foreach($shops as $shop)
                                <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                            @endforeach
                        </select>

                        <div class="invalid-feedback">
                            {{$errors->first('shop')}}
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
