@extends('layouts.app')

@section('section-header', 'Shops')

@section('section-body')
    <div class="card">
        <div class="card-header">
            <h4></h4>
            <div class="card-header-form">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <a href="/employees/create" class="btn btn-primary ml-2">Create employee</a>
        </div>
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Toko</th>
                        <th>Action</th>
                    </tr>
                    @foreach($employees as $employee)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $employee->user->name }}</td>
                            <td>{{ $employee->shop->name }}</td>
                            <td><a href="/employees/{{ $employee->id }}/edit" class="btn btn-icon icon-left btn-info"><i class="fas fa-pencil-alt"></i> Edit</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection