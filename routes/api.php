<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [Controller::class, 'register']);
Route::post('/login', [Controller::class, 'login']);
Route::get('products-shop/{link}', [ProductController::class, 'getProductsByLink']);
Route::get('product/{id}', [ProductController::class, 'getProductById']);
Route::get('shop/{link}', [ShopController::class, 'show']);

Route::middleware('auth:sanctum')->group(function() {
    Route::get('/auth-decode', function (Request $request) {
        return $request->user();
    });

    Route::post('/login-shop', [Controller::class, 'loginShop']);
    Route::post('/logout', [Controller::class, 'logout']);

    Route::resource('shops', ShopController::class);
    Route::resource('orders', OrderController::class);
    Route::resource('discounts', DiscountController::class);
    Route::resource('employees', EmployeeController::class);
    Route::resource('products', ProductController::class);
    Route::resource('product-categories', ProductCategoryController::class);
});
