<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('shop_id')->unsigned();
            $table->bigInteger('product_category_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('desc');
            $table->integer('price');
            $table->integer('stock');
            $table->boolean('show')->default(true);
            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops');
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
