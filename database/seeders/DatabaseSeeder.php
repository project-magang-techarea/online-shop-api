<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use \App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $superadmin = Role::create(['name' => 'superadmin']);
        $admin = Role::create(['name' => 'admin']);
        $user = Role::create(['name' => 'user']);

        Role::create(['name' => 'owner']);
        Role::create(['name' => 'co-owner']);
        Role::create(['name' => 'staff']);
        Role::create(['name' => 'buyer']);

        Permission::create(['name' => 'manipulate user data']);

        $superadmin->givePermissionTo('manipulate user data');

        $admin->givePermissionTo('manipulate user data');

        $superadmin = User::create([
            'name' => 'testing',
            'email' => 'testing@email.com',
            'password' => \Hash::make('123456'),
            'phone_number' => '+6288215035219'
            ]);

        $superadmin->assignRole('superadmin');

        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@email.com',
            'password' => \Hash::make('123456'),
            'phone_number' => '+6288215035218'
            ]);

        $admin->assignRole('admin');

        $user = User::create([
            'name' => 'user',
            'email' => 'user@email.com',
            'password' => \Hash::make('123456'),
            'phone_number' => '+6288215035217'
            ]);

        $user->assignRole('user');
    }
}
