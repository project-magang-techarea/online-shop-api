<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Shop;
use App\Models\Employee;
use DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('shop_id', request()->user()->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $employees
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::role('user')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                        ->from('employees')
                        ->whereColumn('employees.user_id', 'users.id');
            })->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $users
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "user_id" => "required|unique:employees",
            "shop_id" => "required"
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $employee = new Employee;
            $employee->user_id = $request->get('user_id');
            $employee->shop_id = $request->get('shop_id');
            $employee->save();

            if ($employee) {
                $status = "success";
                $message = "add employee successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'add employee failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            "shop_id" => "required"
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $employee = Employee::findOrFail($id);
            $employee->shop_id = $request->get('shop_id');
            $employee->save();

            if ($employee) {
                $status = "success";
                $message = "update employee successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update employee failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
