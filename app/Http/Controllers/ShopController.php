<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shop;
use App\Models\User;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::where('user_id', request()->user()->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $shops
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['link' => \Str::slug($request->get('link'))]);

        $validator = \Validator::make($request->all(), [
            "name" => "required",
            "city" => "required",
            "link" => "required|unique:shops"
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $shop = new Shop;
            $shop->user_id = request()->user()->id;
            $shop->name = $request->get('name');
            $shop->city = $request->get('city');
            $shop->link = \Str::slug($request->get('link'));
            $shop->save();

            $user = User::findOrFail($shop->user_id);
            $user->assignRole('owner');

            if ($shop) {
                $status = "success";
                $message = "add shop successfully";
                $data = $shop;
                $code = 200;
            } else {
                $message = 'add shop failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($link)
    {
        $shop = Shop::where('link', $link)->first();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $shop
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shop = Shop::where('link', \Str::slug($request->get('link')))->first();
        
        if ($shop) {
            if ($shop->id == $id) {
                $oldLink = \Str::slug($request->get('link'));
                $request->merge(['link' => 'me']);
            } else {
                $request->merge(['link' => \Str::slug($request->get('link'))]);
            }
        } 

        $validator = \Validator::make($request->all(), [
            "name" => "required",
            "city" => "required",
            "link" => "required|unique:shops"
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $shop = Shop::findOrFail($id);
            $shop->user_id = 3;
            $shop->name = $request->get('name');
            $shop->city = $request->get('city');

            if ($request->get('link') == 'me') {
                $shop->link = $oldLink;
            } else {
                $shop->link = \Str::slug($request->get('link'));
            }
    
            if($request->file('logo')){
                if($shop->logo && file_exists(storage_path('app/public/' . $shop->logo))){
                    \Storage::delete('public/' . $shop->logo);
                }
    
                $file = $request->file('logo')->store('logo', 'public');
                $shop->logo = $file;
            }
    
            $shop->save();

            if ($shop) {
                $status = "success";
                $message = "update shop successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update shop failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
