<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->where('shop_id', request()->user()->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $products
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'desc' => 'required|string',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'product_category_id' => 'integer',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $product = new Product;
            $product->product_category_id = $request->get('product_category_id');
            $product->shop_id = request()->user()->id;
            $product->name = $request->get('name');
            $product->desc = $request->get('desc');
            $product->price = $request->get('price');
            $product->stock = $request->get('stock');

            if ($request->file('image')) {
                $product->image = $request->file('image')->store('products', 'public');
            }

            $product->save();

            if ($product) {
                $status = "success";
                $message = "add product successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'add product failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('shop_id', request()->user()->id)->findOrFail($id);

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $product
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'desc' => 'required|string',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'product_category_id' => 'integer',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'show' => 'required|boolean'
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $product = Product::findOrFail($id);
            $product->product_category_id = $request->get('product_category_id');
            $product->shop_id = request()->user()->id;
            $product->name = $request->get('name');
            $product->desc = $request->get('desc');
            $product->price = $request->get('price');
            $product->stock = $request->get('stock');
            $product->show = $request->get('show');

            if ($request->file('image')) {
                if ($product->image && file_exists(storage_path('app/public/' . $product->image))) {
                    Storage::delete('public/' . $product->image);
                }

                $product->image = $request->file('image')->store('products', 'public');
            }

            $product->save();

            if ($product) {
                $status = "success";
                $message = "update product successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update product failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function getProductsByLink($link) {
        $shop = Shop::where('link', $link)->first();
        $products = Product::where('shop_id', $shop->id)->where('show', true)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $products
        ], 200);
    }

    public function getProductById($id)
    {
        $product = Product::findOrFail($id);

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $product
        ], 200);
    }
}
