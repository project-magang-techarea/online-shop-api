<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderProduct;
use App\Models\Discount;
use DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('shop_id', request()->user()->id)->orderBy('created_at', 'desc')->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $orders
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'phone_number' => 'required'
        ]);
  
        $status = "error";
        $message = "";
        $data = null;
        $code = 400;
        $error = false;
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $carts = json_decode($request->get('carts'), true);

            $order = new Order;
            $order->shop_id = request()->user()->id;
            $order->buyer_phone_number = $request->get('phone_number');
            $order->trx_number = date('YmdHis');
            $order->status = 'PENDING';
            $order->total_price = 0;

            DB::beginTransaction();
            if ($order->save()) {
                $total_price = 0;

                foreach($carts as $cart) {
                    $id = (int) $cart['id'];
                    $quantity = (int) $cart['quantity'];
                    $product = Product::find($id);

                    if ($product) {
                        if ($product->stock >= $quantity) {
                            $total_price += $product->price * $quantity;

                            $order_product = new OrderProduct;
                            $order_product->order_id = $order->id;
                            $order_product->product_id = $product->id;
                            $order_product->quantity = $quantity;

                            if ($order_product->save()) {
                                $product->stock -= $quantity;
                                $product->save();
                            }
                        } else {
                            $error = true;
                            $message = "out of stock";
                        }
                    } else {
                        $error = true;
                        $message = "product not found";
                    }
                }

                if ($request->has('discount_id') && $request->get('discount_id') !== null) {
                    $discount = Discount::find($request->get('discount_id'));
                    if ($discount) {
                        $order->discount_id = $discount->id;
                        $total_price -= $discount->value * $total_price / 100;
                    } else {
                        $error = true;
                        $message = "discount not found";
                    } 
                }

                $order->total_price = $total_price;

                if ($order->save()) {
                    if(!$error) {
                        DB::commit();

                        $status = "success";
                        $message = "order successfully";
                        $data = null;
                        $code = 200;
                    }
                }
            } else {
                $message = 'order failed';
            }
        }
  
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $order = Order::where('shop_id', request()->user()->id)->findOrFail($id);
            $order->status = $request->get('status');

            $order->save();

            if ($order) {
                $status = "success";
                $message = "update order successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update order failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
