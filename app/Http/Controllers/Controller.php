<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 302;

        $user = User::where('email', '=', $request->get('email'))->first();

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else if ($user) {
            if (Hash::check($request->get('password'), $user->password)) {
                $throw = $user->createToken('user');

                $status = 'success';
                $message = 'Login sukses';
                $data = $throw->plainTextToken;
                $code = 200;
            } else {
                $message = "Login gagal, password salah";
            }
        } else {
            $message = "Login gagal, user " . $request->email . " tidak ditemukan";
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required|max:100",
            "email" => "required|email|unique:users",
            "phone_number" => "required|unique:users",
            "password" => "required",
            "password_confirmation" => "required|same:password"
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 302;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $user = new User;
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone_number = $request->get('phone_number');
            $user->password = Hash::make($request->get('password'));
            $user->save();

            $user->assignRole('user');

            $status = 'success';
            $message = 'Register sukses';
            $code = 200;
            $data = $user;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function loginShop(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required|integer',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 302;

        $user = request()->user();
        $shop = Shop::where('id', '=', $request->get('shop_id'))->where('user_id', '=', $user->id)->first();
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else if ($shop) {
            $throw = $shop->createToken('user');

            $status = 'success';
            $message = 'Login sukses';
            $data = $throw->plainTextToken;
            $code = 200;
        } else {
            $message = "Login gagal, toko tidak ditemukan";
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function logout()
    {
        $user = request()->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'User logged out',
        ], 200);
    }
}
